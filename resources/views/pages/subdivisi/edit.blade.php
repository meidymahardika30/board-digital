@extends('layouts.temp')
@section('icon')
     <i class="feather icon-plus bg-c-blue"></i>
@endsection
@section('title')
    Edit Sub Divisi
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#!">SubDivisi</a> </li>
    <li class="breadcrumb-item"><a href="#!">Edit Sub Divisi</a> </li>
@endsection
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header">
        </div>
        <div class="card-body">
            <form action="{{route('subdivisi.update',$data)}}" method="post">
                @csrf @method('patch')
                <div class="form-group">
                    <label for="my-input">Nama</label>
                    <input id="my-input" class="form-control" type="text" name="name" required value="{{$data->name}}"> 
                </div>
                <div class="form-group">
                    <label for="my-select">Divisi</label>
                    <select id="my-select" class="form-control" name="divisi_id" required>
                        @foreach ($divisi as $item)
                            <option value="{{$item->id}}"
                            @if ($item->id == $data->divisi_id)
                                selected
                            @endif    
                            >{{ $item->name }}</option>
                        @endforeach
                    </select>
                </div>
                <button type="submit" class="btn btn-success">Simpan</button>
                <a href="{{route('divisi.index')}}">
                    <button type="button" class="btn btn-secondary">
                        Kembali
                    </button>
                </a>
            </form>
        </div>
    </div>
</div>
@endsection
