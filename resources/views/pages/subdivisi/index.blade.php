@extends('layouts.temp')
@section('icon')
     <i class="feather icon-tag bg-c-blue"></i>
@endsection
@section('title')
    Sub Divisi
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#!">Sub Divisi</a> </li>
@endsection
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <H3>SubDivisi
            <a href="{{route('subdivisi.create')}}" class="btn btn-success btn-sm float-right">Tambah</a>
            </H3>
        </div>
    </div>

    <div class="card mb-3 comp-card">
        <div class="card-body">
            {{-- <div class="row align-items-center">
                <div class="col">
                    <h3 class=" f-w-700 text-c-black">{{ $show->name }}
                    <span class="text-secondary"><p>{{ $show->divisi }}</p></span></h3>
                    
                </div>
                <div class="col-auto">
                    <a href="{{route('subdivisi.edit',$show)}}" class="btn btn-primary btn-sm waves-effect waves-light text-white"><span class="fas fa-edit"></span></a>
                    <a  class="btn btn-danger btn-sm waves-effect waves-light text-white"
                    onclick="event.preventDefault();
                    $('#delete-form').attr('action','{{route('subdivisi.destroy',$show)}}')
                    document.getElementById('delete-form').submit();"
                    ><span class="fas fa-trash"></span></a>
                    <form id="delete-form" action="" method="POST" style="display: none;">
                        @csrf @method('delete')
                    </form>
                </div>
            </div> --}}

            <div class="table-responsive">
                <table id="datatable" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Divisi</th>
                            <th>Subdivisi</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $show)
                        <tr>
                            <td>{{ $loop->index+1 }}</td>
                            <td>{!! GetDivisi($show->divisi_id) !!}</td>
                            <td>{{ $show->name }}</td>
                            <td>
                                <a href="{{route('subdivisi.edit',$show)}}" class="btn btn-primary btn-sm waves-effect waves-light text-white"><span class="fas fa-edit"></span></a>
                                <a href="{{route('subdivisi.destroy',$show)}}" class="btn btn-danger btn-sm waves-effect waves-light text-white"
                                onclick="return confirm('Apakah anda yakin?')"
                                ><span class="fas fa-trash"></span></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

                {{ $data->links() }}

            </div>
        </div>
    </div>

</div>
@endsection
