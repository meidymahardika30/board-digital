@extends('layouts.temp')
@section('icon')
<i class="feather icon-info bg-c-blue"></i>
@endsection
@section('title')
Detail Event
@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="#!">Event</a> </li>
<li class="breadcrumb-item"><a href="#!">Detail Event</a> </li>
@endsection
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <H3>Detail Event
                <a href="{{route('event.index')}}" class="btn btn-secondary btn-sm float-right">kembali</a>
            </H3>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <div class="card-header-left">
                <h4> Event</h4>
            </div>
            <div class="card-header-right">
                <ul class="list-unstyled card-option">
                    <li class="first-opt"><i class="feather icon-chevron-left open-card-option"></i></li>
                    <li><i class="feather icon-maximize full-card"></i></li>
                    <li><i class="feather icon-minus minimize-card"></i></li>
                    <li><i class="feather icon-refresh-cw reload-card"></i></li>
                    
                    <li><i class="feather icon-chevron-left open-card-option"></i></li>
                </ul>
            </div>
        </div>
        <div class="card-block">
            <table class="table table-responsive">
                <tr>
                    <th>Author</th>
                    <td>:</td>
                    <td>{{ $data->user->name }}</td>
                </tr>
                <tr>
                    <th>Judul</th>
                    <td>:</td>
                    <td>{{ $data->title }}</td>
                </tr>
                <tr>
                    <th>Tipe</th>
                    <td>:</td>
                    <td>{{ $data->type }}</td>
                </tr>
                <tr>
                    <th>Tanggal Mulai</th>
                    <td>:</td>
                    <td>{{ $data->start }}</td>
                </tr>
                <tr>
                    <th>Tanggal Akhir</th>
                    <td>:</td>
                    <td>{{ $data->end }}</td>
                </tr>
                <tr>
                    <th>Status</th>
                    <td>:</td>
                    <td>
                        @if ($data->status == 'waiting')
                        <div class="badge badge-secondary">Menunggu Dikonfirmasi</div>   
                        @elseif ($data->status == 'approve')
                        <div class="badge badge-info">Sudah Dikonfirmasi</div>    
                        @elseif ($data->status == 'rejected')
                        <div class="badge badge-danger">Pengajuan Ditolak</div>    
                        @elseif ($data->status == 'proccess')
                        <div class="badge badge-warning">Berlangsung</div> 
                        @elseif ($data->status == 'done')
                        <div class="badge badge-success">Selesai</div>    
                        @endif
                        
                    </td>
                </tr>
            </table>
            <br>
            @if (Auth::user()->role == 'admin')
                @if ($data->status == 'waiting')
                    <div class="row">
                        <div class="col-md-6">
                            <form action="{{route('approve',$data->id)}}" method="post">
                                @csrf
                                <button type="submit" class="btn btn-primary btn-block"><i class="fas fa-check-circle"></i>Approve</button>
                            </form>
                        </div>
                        <div class="col-md-6">
                            <form action="{{route('reject',$data->id)}}" method="post">
                                @csrf
                                <button type="submit" class="btn btn-danger btn-block"><i class="fas fa-close"></i>Reject</button>
                            </form>
                        </div>
                    </div>    
                @endif
            @elseif (Auth::user()->role == 'user')
                
            @endif
            
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <h4>Lampiran
                @if (Auth::user()->id == $data->user_id || Auth::user()->role == 'admin')
                <a 
                class="btn btn-success waves-effect btn-sm float-right"
                data-toggle="modal"
                data-target="#default-Modal"><i class="text-white fa fa-plus-circle"></i></a>
                    
                @endif
                
                <div class="modal fade" id="default-Modal" tabindex="-1"
                role="dialog">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title"> Tambah lampiran</h4>
                                <button type="button" class="close"
                                    data-dismiss="modal"
                                    aria-label="Close">
                                    <span
                                        aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form action="{{route('attach.store')}}" method="POST" enctype="multipart/form-data">
                                @csrf
                            <div class="modal-body">
                                <label for="">Lampiran </label>
                                <input type="hidden" name="event_id" value="{{$data->id}}">
                                <input required type="file" class="dropify" data-height="300" name="attachment" data-max-file-size="10M" data-allowed-file-extensions="pdf png jpg jpeg doc docx xls xlxs"  />
                                <br>
                            </div>
                            <div class="modal-footer">
                                <button type="button"
                                    class="btn btn-default waves-effect "
                                    data-dismiss="modal">Close</button>
                                <button type="submit"
                                    class="btn btn-success waves-effect waves-light ">Simpan</button>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </h4>
        </div>
        <div class="card-body">
            <div class="row">
                <table class="table">
                    @foreach ($attachment as $item)
            
                    <tr>
                        <td><h4>{{$loop->index+1}}. {{ $item->path }}</h4></td>
                        @if (Auth::user()->id == $data->user_id || Auth::user()->role == 'admin')
                        <td><a href="{{asset('attachment/'.$item->path)}}" download class="btn btn-warning btn-round btn-sm waves-effect waves-light text-white"><span class="fas fa-download"></span></a></td>
                            
                        @endif
                        
                    </tr>
        
                    @endforeach
                </table>
            
            </div>
        </div>
    </div>

    <div class="card">
            <div class="card-header">
                <h4>Laporan
                    @if (Auth::user()->id == $data->user_id || Auth::user()->role == 'admin')
                    <a 
                    class="btn btn-success waves-effect btn-sm float-right"
                    data-toggle="modal"
                    data-target="#report"><i class="text-white fa fa-plus-circle"></i></a>
                        
                    @endif
                    
                    <div class="modal fade" id="report" tabindex="-1"
                    role="dialog">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title"> Tambah Laporan</h4>
                                    <button type="button" class="close"
                                        data-dismiss="modal"
                                        aria-label="Close">
                                        <span
                                            aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form action="{{route('reportevent.store')}}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                <div class="modal-body">
                                    <label for="">Laporan </label>
                                    <input type="hidden" name="event_id" value="{{$data->id}}">
                                    <input required type="file" class="dropify" data-height="300" name="report" data-max-file-size="10M" data-allowed-file-extensions="pdf doc docx xls xlxs"  />
                                    <br>
                                </div>
                                <div class="modal-footer">
                                    <button type="button"
                                        class="btn btn-default waves-effect "
                                        data-dismiss="modal">Close</button>
                                    <button type="submit"
                                        class="btn btn-success waves-effect waves-light ">Simpan</button>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </h4>
            </div>
            <div class="card-body">
                <div class="row">
                    <table class="table">
                        @foreach ($report as $item)
                
                        <tr>
                            <td><h4>{{$loop->index+1}}. {{ $item->path }}</h4></td>
                            @if (Auth::user()->id == $data->user_id || Auth::user()->role == 'admin')
                            <td><a href="{{asset('report/'.$item->path)}}" download class="btn btn-warning btn-round btn-sm waves-effect waves-light text-white"><span class="fas fa-download"></span></a></td>
                                
                            @endif
                            
                        </tr>
            
                        @endforeach
                    </table>
                
                </div>
            </div>
        </div>

</div>
@endsection