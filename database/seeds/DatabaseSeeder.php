<?php

use Illuminate\Database\Seeder;
use App\Divisi;
use App\Subdivisi;
use App\User;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // example : $this->call(UsersTableSeeder::class);
        $this->call(DivisiTableSeeder::class);
        $this->call(SubDivisiTableSeeder::class);

        User::create([
            'name'=>'admin',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('12345678'),
            'role' => 'admin',
            'subdivisi_id' => 0,

        ]);
        
        User::create([
            'name'=>'user_pembina_pramuka',
            'email' => 'userpp@gmail.com',
            'password' => Hash::make('12345678'),
            'role' => 'user',
            'subdivisi_id' => 13,
        ]);

        
    }
}
