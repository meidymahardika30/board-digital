<?php

use Illuminate\Database\Seeder;
use App\Subdivisi;

class SubDivisiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// Kurikulum
        Subdivisi::create([
        	'divisi_id' => 1,
            'name' => 'Waka. Kurikulum'
        ]);

        Subdivisi::create([
        	'divisi_id' => 1,
            'name' => 'Ka. Komp. Li APK'
        ]);

        Subdivisi::create([
        	'divisi_id' => 1,
            'name' => 'Ka. Komp. Li RPL'
        ]);

        Subdivisi::create([
        	'divisi_id' => 1,
            'name' => 'Ka. Komp. Li TKJ'
        ]);

        Subdivisi::create([
        	'divisi_id' => 1,
            'name' => 'Ka. Komp. Li MMD'
        ]);

        Subdivisi::create([
        	'divisi_id' => 1,
            'name' => 'Ka. Komp. Li PMN'
        ]);

        Subdivisi::create([
        	'divisi_id' => 1,
            'name' => 'Ka. Komp. Li HTL'
        ]);

        Subdivisi::create([
        	'divisi_id' => 1,
            'name' => 'Ka. Komp. Li TBG'
        ]);

        Subdivisi::create([
        	'divisi_id' => 1,
            'name' => 'Koordinator Mapel'
        ]);

        Subdivisi::create([
        	'divisi_id' => 1,
            'name' => 'Guru Mapel'
        ]);

        // Kesiswaan
        Subdivisi::create([
        	'divisi_id' => 2,
            'name' => 'Waka. Kesiswaan'
        ]);

        Subdivisi::create([
        	'divisi_id' => 2,
            'name' => 'Pembina Akhlak Mulia'
        ]);

        Subdivisi::create([
        	'divisi_id' => 2,
            'name' => 'Pembina Pramuka'
        ]);

        Subdivisi::create([
        	'divisi_id' => 2,
            'name' => 'Pembina Latih Pramuka'
        ]);

        Subdivisi::create([
        	'divisi_id' => 2,
            'name' => 'Pembina OSIS'
        ]);

        Subdivisi::create([
        	'divisi_id' => 2,
            'name' => 'Koordinator BK dan Pengembangan Diri'
        ]);

        Subdivisi::create([
        	'divisi_id' => 2,
            'name' => 'Koordiantor Ekskul dan Senbud'
        ]);

        Subdivisi::create([
        	'divisi_id' => 2,
            'name' => 'Koordinator Beasiswa'
        ]);

        Subdivisi::create([
        	'divisi_id' => 2,
            'name' => 'Koordinator Infak dan Sodaqoh'
        ]);

        Subdivisi::create([
        	'divisi_id' => 2,
            'name' => 'Pembimbing Siswa'
        ]);

        // Sarana & Prasarana
        Subdivisi::create([
        	'divisi_id' => 3,
            'name' => 'Wakasek Urusan Sarpras'
        ]);

        Subdivisi::create([
        	'divisi_id' => 3,
            'name' => 'Pelaksana Sarpras'
        ]);

        Subdivisi::create([
        	'divisi_id' => 3,
            'name' => 'Kepala Lab & Server'
        ]);

        Subdivisi::create([
        	'divisi_id' => 3,
            'name' => 'Teknisi Perawatan dan Perbaikan'
        ]);

        // Tata Usaha & Humas
        Subdivisi::create([
        	'divisi_id' => 4,
            'name' => 'Wakasek Urusan Humas'
        ]);

        Subdivisi::create([
        	'divisi_id' => 4,
            'name' => 'Pelaksana Keuangan dan Kerumahtanggaan'
        ]);

        Subdivisi::create([
        	'divisi_id' => 4,
            'name' => 'Pelaksana Kepegawaian'
        ]);

        Subdivisi::create([
        	'divisi_id' => 4,
            'name' => 'Pelaksana Kurikulum'
        ]);

        Subdivisi::create([
        	'divisi_id' => 4,
            'name' => 'Pelaksana Kesiswaan'
        ]);

        Subdivisi::create([
        	'divisi_id' => 4,
            'name' => 'Pembantu Umum Bid. Kerumahtanggaan'
        ]);

        // BKK
        Subdivisi::create([
        	'divisi_id' => 5,
            'name' => 'Manajer BKK & Hubinak'
        ]);

        // Pelayanan Data & Informasi
        Subdivisi::create([
        	'divisi_id' => 6,
            'name' => 'Manajer Pedatin'
        ]);

        Subdivisi::create([
        	'divisi_id' => 6,
            'name' => 'Pelaksana Perpustakaan'
        ]);

        Subdivisi::create([
        	'divisi_id' => 6,
            'name' => 'Pelaksana Pedatin'
        ]);

        // Manajemen Mutu
        Subdivisi::create([
        	'divisi_id' => 7,
            'name' => 'Wa. Manajemen Mutu'
        ]);

        // Kesehatan & Lingkungan
        Subdivisi::create([
        	'divisi_id' => 8,
            'name' => 'Manajer Kesling'
        ]);

        Subdivisi::create([
        	'divisi_id' => 8,
            'name' => 'Pembina Pendidikan Berkelanjutan'
        ]);

        Subdivisi::create([
        	'divisi_id' => 8,
            'name' => 'Pembina Unit Kesehatan Sekolah (UKS)'
        ]);

        Subdivisi::create([
        	'divisi_id' => 8,
            'name' => 'Pelaksana Lingkungan'
        ]);
    }
}
